\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{consignemv}[2017/02/20 v.01 Consigne]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{consignemv}}
\ProcessOptions \relax %marche pas

\LoadClass{corpusmv}

\usepackage{lastpage}
\usepackage{datetime}


\geometry{inner=2.5cm,   %2.625cm, 
    outer=1.5cm, %1.375?
    marginparsep=0.7cm, 
    top=2.5cm,%1.8561cm,
    height=22cm, %24.13cm,
    textwidth=12cm,%12.45cm,
    marginparwidth=3.1cm,
    %headheight=
    footskip=1.5cm}
\fancyhead{}\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.0pt}% default is 0pt
\pagestyle{fancy}
%\lfoot{\monmail}
%\rhead{\small\today}
\cfoot{\thepage\,/\,\pageref*{LastPage}}%page 1/5

%\RequirePackage{booktabs}% midrule toprule tableau sans lignes verticales
%\RequirePackage{tabularx}



\newcommand\zpts{{\small0 -- 1 -- 2 -- 3 -- 4 -- 5}}
\newcommand\zptsd{{\small0 -- 2 -- 4 -- 6 -- 8 -- 10}}
\newcommand\monmail{%
\small\href{mailto:pierre.pochon@vd.educanet2.ch?subject=A propos du fichier \jobname}{\texttt{pierre.pochon@vd.educanet2.ch}}
}

\hypersetup{breaklinks=true,colorlinks=false, urlcolor=blue}

\graphicspath{{../../IMG/}{../../IMG/tumbs/}}

%\DeclareFieldFormat
  %[online, periodical, article,inbook,incollection,inproceedings,patent,thesis,unpublished]
  %{url}{\printtext{\href{\thefield{url}}{\lienExterne}}}

% par d'indent au premier paragraphe
\frenchbsetup{IndentFirst=false}

\RequirePackage{titlesec}%lignes sous les sections
    \titleformat{\section}
  {\normalfont\Large\bfseries}{\thesection}{1em}{}[{\titlerule[0.8pt]}]
%https://tex.stackexchange.com/questions/24439/how-to-add-a-dot-after-the-section-number pour la ligne suivante, de titlesec
%\titlelabel{\thetitle)\quad} %separateur
\renewcommand{\thesection}{\Alph{section}}
\renewcommand{\thesubsection}{\arabic{subsection}}

\DeclareBibliographyDriver{periodical}{% 
     \usebibmacro{bibindex}%
     \usebibmacro{begentry}%
     \usebibmacro{title}
     \newunit
     \usebibmacro{institution+location+date}
     \newunit
     %\printfield{addendum}%
     \newunit
     \printfield{url} 
     \newunit
     \printfield{addendum}
 }
 
\etocsettocdepth{2}

