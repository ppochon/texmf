\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{proj}[2015/09/03 v.01 projection d'images]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptions \relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \Titre définit un titre \Soustitre un soustitre  %%
%% \Surtitre un surtitre                            %%
%% APRES BEGIN DOCUMENT:                            %%
%% \Titrage: Page de titre \Table une table         %%
%% \pagetitre{} une frame de titre                  %%
%% \pagetitres{}{} une frame avec soustitre         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\LoadClass[10pt,aspectratio=1610]{beamer}

\input{../chemins.tex} %donne les commandes \bibliox et \imgbiblio

\RequirePackage[frenchb]{babel}
    \frenchbsetup{ThinColonSpace=true} %espace fine après ":"
\RequirePackage[backend=biber,
    datamodel=hist,
    ibidtracker=false,
    idemtracker=false,
    opcittracker=false,
    citestyle=verbose-trad2,
    url=true,
    dateuncertain=true, 
    datecirca=true]{biblatex} 
    \addbibresource{\bibliox}%chemin ref textes
    \addbibresource{\imgbiblio}%chemin ref images
    \addbibresource{docus.bib}% ~/texmf/bibtex/bib/docus.bib
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[babel=true]{microtype}
\RequirePackage{graphicx}
    \graphicspath{{\chemimage}}%chemin des images
\RequirePackage{csquotes}
\RequirePackage{ragged2e}
\RequirePackage{xspace}
\RequirePackage{setspace}%espacement des lignes avec \setstrech{1.3}
\RequirePackage[modulo]{lineno}
\RequirePackage{multicol}
\RequirePackage{tikz}
	\usetikzlibrary{arrows,
        babel,
        backgrounds,
        chains,
        decorations.pathreplacing,
        fadings,
        fit,
        patterns,
        positioning,
        scopes,
        shapes,
        trees,}
\RequirePackage{forest}
\RequirePackage{etoolbox}%ifdef
\RequirePackage{xpatch}
%\RequirePackage{xifthen}%choisir avec etool une fois… EN TEST SANS
\RequirePackage{pgfplotstable} % voir pgfplotstableset dans TABLEAUX
\RequirePackage{booktabs}
\RequirePackage{array}
\RequirePackage{tabularx}
    \def\arraystretch{1.5}
\RequirePackage[detect-none,
    %number-color=green,
    ]{siunitx} %unités et align chiffres dans tableau
\RequirePackage[justification=raggedright,
    format=plain,
    labelformat=empty, % pas de 'Table/Figure 1'
    %singlelinecheck=false, % evite de centrer  les lignes courstes
    ]{caption} %autorise des caption multi-lignes
\RequirePackage{MnSymbol}%caracteres speciaux
\RequirePackage{textcomp}%caracteres speciaux
%\RequirePackage{wasysym}%caracteres speciaux
%\RequirePackage{fontawesome}
\RequirePackage[first=1, last=2]{lcg}%nombre aléatoire avec la commande \rand
\RequirePackage{varwidth} %boites de textes dans frise



%%%%%%%%%%%%%%%%% CARACTERES SPECIAUX %%%%%%%%%%%%%%%%

\newcommand{\lienExterne}{%
    \tikz[x=1.2ex, y=1.2ex, baseline=-0.05ex]{% 
        \begin{scope}[x=1ex, y=1ex]
            \clip (-0.1,-0.1) 
                --++ (-0, 1.2) 
                --++ (0.6, 0) 
                --++ (0, -0.6) 
                --++ (0.6, 0) 
                --++ (0, -1);
            \path[draw, 
                line width = 0.6, 
                rounded corners=0.5] 
                (0,0) rectangle (1,1);
        \end{scope}
        \path[draw, thick] (0.5, 0.5) 
            -- (1, 1);
        \path[draw, line width = 0.6] (0.6, 1) 
            -- (1, 1) -- (1, 0.6);
        }%
    }%

%%%%%%%%%%%%%%%%%% BIBLIO %%%%%%%%%%%%%%%%%%%%%
\RequirePackage{biblio}    

%%%%%%%%%%%%%%%%% DIMENSIONS %%%%%%%%%%%%%%%%%%
\newlength{\largeur}
\newlength{\reste}
%\geometry{paperwidth=200mm,paperheight=125mm} 

%%%%%%%%%%%%%%%% COULEURS %%%%%%%%%%%%%%%%%%%%%
\definecolor{gris-fonce}{gray}{0.25}
\definecolor{frise}{gray}{0.33}
\definecolor{gris-moyen}{gray}{0.35}
\definecolor{gris-discret}{gray}{0.85}
\definecolor{gris-clair}{gray}{0.95}

\definecolor{jaune}{RGB}{225,225,20}  % pâlit en augmentant le bleu
\definecolor{creme}{RGB}{233,221,175}  
\definecolor{ocre}{RGB}{233,182,22}  
\definecolor{tuile}{RGB}{212,85,0}  
\definecolor{brun}{RGB}{117,73,37}  
\definecolor{gris}{RGB}{102,102,102}  
\definecolor{vert}{RGB}{137,160,44}  
\definecolor{ciel}{RGB}{85,221,255}  
\definecolor{outremer}{RGB}{44,90,160}  
\definecolor{ardoise}{RGB}{72,69,83}  

%%%%%%%%%%%%%% BEAMER %%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamertemplate{navigation symbols}{%
    %	\insertframenavigationsymbol
    %   \insertdocnavigationsymbol
	%   \insertbackfindforwardnavigationsymbol
    \insertsectionnavigationsymbol
	}
\setbeamercolor{normal text}{fg=gris-clair}
\setbeamercolor{bibliography entry title}{fg=jaune}
\setbeamercolor{bibliography entry author}{fg=jaune}
\setbeamercolor{bibliography entry note}{fg=jaune}
\setbeamercolor{navigation symbols}{fg=gris-moyen}
\setbeamercolor{navigation symbols dimmed}{fg=gris-moyen}
\setbeamercolor{section in toc}{fg=gris-clair}
\setbeamercolor{subsection in toc}{fg=gris-clair}

\AtEveryCite{\color{jaune}} %mets en jaune les pre et post citation

\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{background canvas}{%
\begin{tikzpicture}
\shade[top color=gris-moyen, bottom color=gris-fonce, shading angle=-18] (0,0) rectangle (16,10);
\end{tikzpicture}
}
\defbeamertemplate{itemize item}{tiret}{\textemdash}%definition
\setbeamertemplate{itemize items}[tiret]%application
\setbeamertemplate{itemize subitems}[tiret]%
\setbeamertemplate{itemize subsubitems}[tiret]%
\setbeamercolor{local structure}{fg=gris-clair}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}%pas de "figure"

\newenvironment{changemargin}[2]{% pour largimage
  \begin{list}{}{% 
    \setlength{\topsep}{0pt}% 
    \setlength{\leftmargin}{#1}% 
    \setlength{\rightmargin}{#2}% 
    \setlength{\listparindent}{\parindent}% 
    \setlength{\itemindent}{\parindent}% 
    \setlength{\parsep}{\parskip}% 
  }% 
  \item[]}{\end{list}} 



%%%%%%%%%%%% IMAGES %%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\largeg}[1]
{\begin{frame}%
\settowidth{\largeur}{\includegraphics[height=\paperheight]{{#1}}}%
\setlength{\reste}{\textwidth} %
\addtolength{\reste}{-\largeur}%
\begin{columns}%
	\begin{column}{\largeur}%
	\includegraphics[height=\paperheight]{{#1}}%
	\end{column}%
	\begin{column}[c]{\reste}%
	\vbox to \textheight{%
		\vspace*{\stretch{10}}%
		\small\cite{#1}%
		\vspace*{\stretch{1}}%
	}% 
	\end{column}
\end{columns}
\end{frame}}

\newcommand{\larged}[1]
{\begin{frame}%
\settowidth{\largeur}{\includegraphics[height=\paperheight]{{#1}}}%
\setlength{\reste}{\textwidth} %
\addtolength{\reste}{-\largeur}%
\begin{columns}%
	\begin{column}[c]{\reste}%
	\vbox to \textheight{%
		\vspace*{\stretch{10}}%
		\small\mdseries\RaggedLeft\cite{#1}%
		\vspace*{\stretch{1}}%
	}% 
	\end{column}%
	\begin{column}{\largeur}%
	\includegraphics[height=\paperheight]{{#1}}%
	\end{column}%
\end{columns}
\end{frame}}

\newcommand{\etroitg}[1]
{\begin{frame}%
\begin{columns}%
	\begin{column}{0.66\paperwidth}%
		\hspace*{\stretch{5}}%
		\includegraphics[height=\paperheight]{{#1}}%
		\hspace*{\stretch{2}}%
	\end{column}%
	\begin{column}[c]{0.24\paperwidth}%
	\vbox to \textheight{%
		\vspace*{\stretch{10}}
		\small\mdseries\cite{#1}%	
		\vspace*{\stretch{1}}%
	} 
	\end{column}
		\begin{column}{0.10\paperwidth}\end{column}%

\end{columns}
\end{frame}}

\newcommand{\etroitd}[1]{%
\begin{frame}%
\begin{columns}%
	\begin{column}{0.10\paperwidth}\end{column}%
	\begin{column}[c]{0.24\paperwidth}%
	\raggedleft%
	\vbox to \textheight{%
		\vspace*{\stretch{10}}%
\small\raggedleft\cite{#1}%
		\vspace*{\stretch{1}}%
	}% 
	\end{column}%
	\begin{column}{0.66\paperwidth}%
		\hspace*{\stretch{2}}%
		\includegraphics[height=\paperheight]{{#1}}%
		\hspace*{\stretch{5}}%
	\end{column}%
	\end{columns}%
\end{frame}}

\newcommand{\tlimage}[1]{%
\begin{frame}%
\begin{changemargin}{-1.3cm}{-1.3cm}\centering
		\includegraphics[width=\paperwidth,height=0.9\paperheight,keepaspectratio]{{#1}}%
		\par\smallskip%
		\begin{minipage}[t]{0.8\paperwidth}%
			\begin{center}
				\footnotesize\mdseries\fullcite{#1}%
			\end{center}
		\end{minipage}
\end{changemargin}	
\end{frame}}

\newsavebox\boite%une boite pour mesurer imgwdth
\newcommand{\largimage}[1]
{\begin{frame}%
    \begin{center}
    \sbox\boite{\includegraphics[width=\textwidth, height=0.88\paperheight,keepaspectratio]{{#1}}}%
\includegraphics[width=\textwidth, height=0.88\paperheight,keepaspectratio]{{#1}}
    \par\medskip%
    \begin{minipage}{\the\wd\boite}
        \footnotesize\mdseries\fullcite{#1}%
    \end{minipage}
\end{center}
\end{frame}}

\newcommand{\petitimage}[1]
{\begin{frame}%
\begin{columns}%
\begin{column}{0.25\paperwidth}
\end{column}
\begin{column}{0.5\paperwidth}
		\vbox to \textheight{%
		\vspace*{\stretch{2}}
		\includegraphics[width=\textwidth]{{#1}}\par\smallskip
		\small\justifying\mdseries\cite{#1}%	
		\vspace*{\stretch{3}}}
\end{column}
\begin{column}{0.25\paperwidth}
\end{column}
\end{columns}%	
\end{frame}}

\newcommand{\petitehaute}[1]
{\begin{frame}%
\begin{columns}%
	\begin{column}{0.66\paperwidth}%
		\hspace*{\stretch{5}}%
		\includegraphics[height=0.8\paperheight]{{#1}}%
		\hspace*{\stretch{2}}
	\end{column}%
	\begin{column}[c]{0.24\paperwidth}%
	\vbox to \textheight{%
		\vspace*{\fill}%
		\small\mdseries\cite{#1}%	
		\vspace*{0.1\paperheight}
	} 
	\end{column}
		\begin{column}{0.10\paperwidth}\end{column}%

\end{columns}
\end{frame}}

\newcommand{\deuximages}[2]
{\begin{frame}%
\begin{columns}%
\begin{column}{0.5\textwidth}
\settowidth{\largeur}{\includegraphics[width=\textwidth,height=0.8\paperheight,keepaspectratio]{{#1}}}%
\centering
		\vbox to \textheight{%
		\vspace*{\stretch{10}}
		\includegraphics[width=\textwidth,height=0.8\paperheight,keepaspectratio]{{#1}}\par\smallskip
		\vspace*{\stretch{3}}
		\begin{minipage}{\largeur}%
		\small\mdseries\cite{#1}%	
		\end{minipage}
		\vspace*{\stretch{20}}}
\end{column}
\begin{column}{0.5\textwidth}
\settowidth{\largeur}{\includegraphics[width=\textwidth,height=0.8\paperheight,keepaspectratio]{{#2}}}%
\centering
		\vbox to \textheight{%
		\vspace*{\stretch{10}}
		\includegraphics[width=\textwidth,height=0.8\paperheight,keepaspectratio]{{#2}}\par\smallskip
		\vspace*{\stretch{3}}
		\begin{minipage}{\largeur}%
		\small\mdseries\cite{#2}%	
		\end{minipage}
		\vspace*{\stretch{20}}}
\end{column}
\end{columns}%	
\end{frame}}


% \newcommand{\gauchetoute}[2]{%
%     \begin{frame}%


%%%%%%%%%%%%%%% GENEALOGIE %%%%%%%%%%%%%%%%%%%%%
\newenvironment{arbre}{%
\begin{tikzpicture}[%
      man/.style={minimum height = 3em, text width=7em, align=center},
      woman/.style={minimum height = 3em, align=center}%
      ]%
}%      
{\end{tikzpicture}}

%%%%%%%%%%%% FRISES %%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{frise}

%%%%%%%%%%%% COMMANDES %%%%%%%%%%%%%%%%%%%%%%%%
\newlength{\imagewidth}%nouvelle longueur pour la largeur
\newlength{\imageheight}%nouvelle longueur pour la hauteur

\newcommand\iproj[1]{%
\settowidth{\imagewidth}{\includegraphics[]{#1}}%calcul de la largeur->2016 [resolution=150] était ajouté
\settoheight{\imageheight}{\includegraphics[]{#1}}%calcul de la hauteur ->2016 [resolution=150] était ajouté
\ifdim\imagewidth>\imageheight %on commence par les images horizontales
    \ifdim\imagewidth<500pt %on repère les petites
        \petitimage{#1}
    \else %elle es plus large que 16/10
        \ifdim\imagewidth>1.6\imageheight 
	      \tlimage{#1}%	
        \else
            \ifdim\imagewidth>1.2\imageheight 
                \largimage{#1}%
            \else
                \rand %coup de sac entre 1 et 2
                \ifnum \arabic{rand}=1 %si c'est 1
                    \largeg{#1}%
                \else 
                    \larged{#1}%
                \fi
            \fi
        \fi
    \fi
\else %l'image est verticale
    \ifdim\imageheight<500pt %elle est petite
        \petitehaute{#1} 
    \else	  
        \ifdim\imagewidth>0.85\imageheight
    	  \rand
            \ifnum \arabic{rand}=1
                \largeg{#1}%
		\else
		    \larged{#1}%
		\fi
        \else
        \rand
             \ifnum \arabic{rand}=1
                \etroitg{#1}%
		\else
		    \etroitd{#1}%
		\fi
        \fi
    \fi
\fi
}




%%%%%%%%%%%%%%%%%%% TEXTES %%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\courtexte}[2][0.72]{%insérer un court texte tiré de CORPUS
\begin{frame}
\center\begin{minipage}{#1\linewidth}% la largeur s'exprime en par opt en frct de linewidth
\input{../CORPUS/#2}
\end{minipage}
\end{frame}
}
\newenvironment{texte}[1][1.2]
{\begin{internallinenumbers*}\setlength\parsep{15pt}\setstretch{#1}}
{\end{internallinenumbers*}}


\newenvironment{refe}
{\smallskip\color{jaune}}{}

\newcommand{\italic}[1]{{\setstretch{1.0}\emph{#1}}}

\let\oldfootnote\footnote
\renewcommand{\footnote}[1]{% texte des notes en jaune
    \oldfootnote{\color{jaune}#1}
    }
%minipages footnotes
\renewcommand\thempfootnote{\textcolor{jaune}{\arabic{mpfootnote}}}


%%%%%%%%%%%%%%%%%%% EXERCICES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcounter{exercice}
\newcommand\exercice{%
    \stepcounter{exercice}{\Large Exercice~\arabic{exercice}}
    \vspace{-1ex}\par
    \hrulefill
    \vspace{1ex}
    }

\newcommand\consigne[2][0.72]{%
    \begin{frame}
    \center\begin{minipage}{#1\linewidth}% la largeur s'exprime en par opt en frct de linewidth
        \exercice\par
        \input{#2}
    \end{minipage}
    \end{frame}
    }

%%%%%%%%%%%%%%%%%%% TITRES %%%%%%%%%%%%%%%%%%%%%%%%%%

\providecommand\Titrage{%
    \begin{frame} 
        \center{%
        \begin{minipage}{0.95\linewidth}%
        \vspace{8em}
            \ifdef{\@Surtitre}{\center\Large\mdseries{\@Surtitre}\vspace{-1ex}}{}%
            \ifdef{\@Titre}{\center\Huge\mdseries{\@Titre}}{}%
            \ifdef{\@Soustitre}{\center\Large\mdseries{\@Soustitre}}{}% 
            
              \RaggedLeft{\vspace{8em}\color{jaune}\small\RaggedRight Signaler une erreur\,: \href{mailto:pierre.pochon@vd.educanet2.ch?subject=ERREUR dans le fichier \jobname}{\texttt{pierre.pochon@vd.educanet2.ch}}} 
            \end{minipage}}
           
    \end{frame}
}
\newcommand{\Titre}{\newcommand{\@Titre}}
\newcommand{\Soustitre}{\newcommand{\@Soustitre}}
\newcommand{\Surtitre}{\newcommand{\@Surtitre}}

\newcounter{titreno}

\newcommand{\pagetitre}[2][0.66]{% Une page avec un titre
    \section{#2}    
    \begin{frame} 
        \center\begin{minipage}{#1\linewidth}%
            \center\Huge{\stepcounter{titreno}\thetitreno. #2}
        \end{minipage} 
    \end{frame}
    } 
\newcommand{\pagetitres}[3][0.66]{% Une page avec titre et soustitre
    \section{#2}
    \begin{frame} 
        \center\begin{minipage}{#1\linewidth}%
            \center\Huge{\stepcounter{titreno}\thetitreno. #2}
            \center\Large{#3}
        \end{minipage} 
    \end{frame}
    } 
\newcommand{\pagesoustitre}[2][0.66]{% Une page avec titre et soustitre
    \subsection{#2}
    \begin{frame} 
        \center\begin{minipage}{#1\linewidth}%
            \center\Huge{#2}
        \end{minipage} 
    \end{frame}
    } 
\newcommand{\pagesoustitres}[3][0.66]{% Une page avec titre et soustitre
    \subsection{#2}
    \begin{frame} 
        \center\begin{minipage}{#1\linewidth}%
            \center\Huge{#2}
            \center\Large{#3}
        \end{minipage} 
    \end{frame}
    } 

%%%%%%%%%%%%%%%%% TABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\Table{%
    \frame{%
        \center\begin{minipage}{0.66\linewidth}
            \setstretch{1.2}\RaggedRight\large{\tableofcontents}
        \end{minipage}
        }
    }

\newcommand\Tablel{% Longues tables des matières
    \begin{frame}
        \center\begin{minipage}{0.77\linewidth}
        \begin{multicols}{2}
            \tableofcontents
        \end{multicols}
        \end{minipage}
    \end{frame}
    }

\newcommand{\coupetable}{% ds le texte, force un saut de col avec Tablel 
    \addtocontents{toc}{\newpage}
    }

%%%%%%%%%%%%%%%%%%%% TABLEAUX %%%%%%%%%%%%%%%%%%%%%%%%%
\newcolumntype{i}{>{\fontfamily{SourceSansPro-TLF}\selectfont}r}%ne marche pas pour l'instant, enfin seulement pour les string  grrr package array
\pgfplotstableset{%
    search path={.,../../../DAT/}, %recherche des données
    column type=r,
    1000 sep={\,},
    fixed, % pas de notation scientifique
    every head row/.style={
        before row=\toprule,
        after row=\hline,
        },   
    every last row/.style={
        after row=\bottomrule,
        },
    }
    
\newcommand{\refnotebox}{}%
\newcommand\refnote[2][]{%commande pour la référence
    \renewcommand\refnotebox{\footnotesize{\fullcite[#1]{#2}}}
    }

% \newcommand{\tabfootloader}{}%
% \newcommand\tabfoot[3]{%pour les notes des tableaux (ne marche pas)
%     \renewcommand\tabfootloader{%
%         every row #1 column #2/.style={postproc cell content/.style={/pgfplots/table/@cell content/.add={}{\footnote{#3}}}}%
%         }%
%     }%

\newsavebox{\btab}%boite pour mesurer les tableaux
\newcommand\tablo[3][]{%
    \sbox\btab{\pgfplotstabletypeset[#1]{#3}}% savebox mesure le tableau
    \ifdim\the\wd\btab>0.4\textwidth %s'il est plus large que 0.4\textwidth
        \begin{frame}
        \begin{center}                            
        \begin{minipage}{\the\wd\btab}
            #2

            \smallskip%ne marche pas sans l'espace

            \pgfplotstabletypeset[#1]{#3}
            \smallskip

            \refnotebox
        \end{minipage}
        \end{center}
        \end{frame}
    \else %il est plus étroit que 0.4\textwidth
        \begin{frame}
        \begin{center}                            
        \begin{minipage}{0.5\textwidth}
            \centering
            #2

            \smallskip%ne marche pas sans l'espace

            \pgfplotstabletypeset[#1]{#3}
            \smallskip

            \refnotebox
        \end{minipage}
        \end{center}
        \end{frame}
    \fi
    }             
%%%%%%%%%%%%%%%% VIDEOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\video[3][]{%mettre les affiches en .jpg 
    \begin{frame}
        \IfFileExists{../IMG/#3.jpg}{%true affiche+ref
            \begin{columns}%
	            \begin{column}{0.66\paperwidth}%
		            \hspace*{\stretch{5}}%
                    \href{run:#2}{%
		                \includegraphics[height=\paperheight]{{#3}}%
                        }%
		            \hspace*{\stretch{2}}%
	            \end{column}%
	        \begin{column}[c]{0.24\paperwidth}%
	            \vbox to \textheight{%
		        \vspace*{\stretch{10}}
                \href{run:#2}{%
		            \small\mdseries\cite[#1]{#3}%
                    }%
		        \vspace*{\stretch{1}}%
	            } 
	        \end{column}
		    \begin{column}{0.10\paperwidth}\end{column}%
            \end{columns}
        }{%false juste la ref
            \noindent\makebox[\textwidth][c]{%
                \begin{minipage}{0.6\textwidth}
                \hrulefill\par
                    \vspace{1ex}\href{run:#2}{\cite[#1]{#3}}
                    \par
                \hrulefill
                \end{minipage}
            }
        }
    \end{frame}
    }

%%%%%%%%%%%%%%%% FONTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\mrd{\textrm{\textmarried}\xspace}
\RequirePackage{petitesmacros}
\RequirePackage[proportional, tabular,semibold]{sourcesanspro}%options: semibold, light qui transforme \mdseries, osf: chiffres old
\RequirePackage[scaled=1]{inconsolata}
%\RequirePackage[light]{roboto}
%\RequirePackage{droid}
%\RequirePackage{PTSerif}
%\renewcommand{\familydefault}{\sfdefault}
%\usefonttheme{serif}
\RequirePackage{hyperref}
\hypersetup{breaklinks=true,colorlinks=false,urlcolor=blue}

