\NeedsTeXFormat{LaTeX2e}%/!\ pas fini
\ProvidesClass{lettrelua}[2017-02-06 lettre]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{chletter}}
\ProcessOptions \relax

\LoadClass[11pt]{chletter}

\RequirePackage[french]{babel}
    \frenchbsetup{ThinColonSpace=true}
% \RequirePackage[expert]{fourier}
% \RequirePackage[scaled=0.875]{helvet}
% \RequirePackage{courier}
% \RequirePackage[utf8]{inputenc}
% 
% \usepackage{luatextra} % charge fontspec
\RequirePackage[no-math]{fontspec}
    \setmainfont[Path=/home/pierre/.fonts/,
        Ligatures=TeX, %pour les postrophes
        BoldFont={NeueHaasUnicaPro-Regular.ttf}, 
        ItalicFont={NeueHaasUnicaPro-LightIt.ttf},
        BoldItalicFont={NeueHaasUnicaPro-Italic.ttf}]{NeueHaasUnicaPro-Light.ttf}
    \setmonofont[Scale=1.12]{TeX Gyre Cursor}
\newfontfamily\policetexte[Path=/home/pierre/.fonts/, 
        Ligatures=TeX,
        BoldFont={NeueHaasUnicaPro-Bold.ttf}, 
        ItalicFont={NeueHaasUnicaPro-Italic.ttf},
        BoldItalicFont={NeueHaasUnicaPro-BoldItalic.ttf}]{NeueHaasUnicaPro-Regular.ttf}
\newfontfamily\policerefe[Path=/home/pierre/.fonts/, 
        Ligatures=TeX,
        BoldFont={NeueHaasUnicaPro-Regular.ttf}, 
        ItalicFont={NeueHaasUnicaPro-LightIt.ttf},
        BoldItalicFont={NeueHaasUnicaPro-Italic.ttf}]{NeueHaasUnicaPro-Light.ttf}
%\setromanfont{Verdana}
%\setmonofont{DejaVu Sans Mono}
\newcommand{\textsb}[1]{% pas de Fourrier pas de textsb
     \textbf{#1}%
     }
\renewcommand{\textsc}[1]{% pas de Fourrier pas de smallcaps
     \MakeUppercase{#1}%
     }

\RequirePackage{etoolbox}

\RequirePackage{xspace}

\RequirePackage[babel=true]{microtype}
	\widowpenalty=9999
	\clubpenalty=9999
\RequirePackage[autolanguage]{numprint}


\RequirePackage{petitesmacros}

%\date{Lausanne, le \today}


