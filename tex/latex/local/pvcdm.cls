\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{pv}[2016/10/17 v.01 base pour un proces verbal]
% Les commandes \Date \Titre \Lieu \Presents \Excuses 
% doivent être définies avant \begin{document}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions \relax

\LoadClass[hidelinks,11pt, a4]{article}

\RequirePackage[french]{babel}
    \frenchbsetup{ThinColonSpace=true}
\RequirePackage[expert]{fourier}
\RequirePackage{courier}
\RequirePackage[utf8]{inputenc}
\RequirePackage[babel=true]{microtype}
\RequirePackage{xspace,csquotes,lipsum,array}
    \renewcommand{\arraystretch}{1.5}
\RequirePackage{graphicx}
\RequirePackage{xfrac}
\RequirePackage{tabularx}
\RequirePackage{multicol}
\RequirePackage{ragged2e}
\RequirePackage{enumitem} %ajuste description avec [leftmargin=0cm]
\RequirePackage{parskip} %supprime les indent et separe les par.
\RequirePackage{ifthen} %titre seulement sur la 1re page
\RequirePackage{fancyhdr}
    \pagestyle{fancy}
    %%\renewcommand{\headrulewidth}{0pt}
    \lhead{\ifthenelse{\value{page}=1}{}{}}
    \rhead{Procès-verbal du \@Date}
    \cfoot{\thepage\,/\,\pageref{LastPage}}
    \rfoot{\ifthenelse{\value{page}=\pageref{LastPage}}
    {\small{\href{mailto:pierre.pochon@vd.educanet2.ch?subject=\jobname}{Pierre Pochon}, le \today}}{}}
\RequirePackage{lastpage}
\RequirePackage{titlesec}
    \titleformat*{\section}{\large\bfseries}
    \titlespacing*{\section}{0pt}%identation
        {2ex plus 1ex minus 0.2ex}%espace vert avant
        {0.5ex plus 0ex minus 0.2ex}%espace vert après
\RequirePackage{url,hyperref}
    \hypersetup{colorlinks,citecolor=black,filecolor=black,linkcolor=black,urlcolor=blue}
\widowpenalty=9999\clubpenalty=9999%veuves et orphelins

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Commandes%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\Lieu}[1]{\def\@Lieu{#1}}
\newcommand{\Date}[1]{\def\@Date{#1}}
\newcommand{\Titre}[1]{\def\@Titre{#1}}
\newcommand{\Presents}[1]{\def\@Presents{#1}}
\newcommand{\Excuses}[1]{\def\@Excuses{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Tableau_debut%%%%%%%%%%%%%%%%%%%%%%%%
% \AtBeginDocument{% ecrit un tableau lieu-presents-excusés au début
%     \begin{tabularx}{\linewidth}{@{}>{\bfseries}lX@{}}
%         \ifdef{\@Lieu}{Lieu\,:       & \@Lieu \\}{}
%         \ifdef{\@Presents}{Présents\,:   &  \@Presents}{} \\
%         \ifdef{\@Excuses}{Excusés\,:    & \@Excuses}{} \\
%     \end{tabularx}
% }


