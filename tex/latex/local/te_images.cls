\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{te_images}[2016/05/26 v.01 travail avec images]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions \relax

\LoadClass[hidelinks,12pt]{article}

\input{../../chemins.tex}

\RequirePackage[french]{babel}
    \frenchbsetup{ThinColonSpace=true}
\RequirePackage[backend=biber,citestyle=verbose-trad2,bibstyle=verbose-trad3,url=false,ibidtracker=false]{biblatex}
    \addbibresource{\bibliox}%chemin ref textes
    \addbibresource{\imgbiblio}%chemin ref images
\ExecuteBibliographyOptions[reference]{useeditor=false}%dicos: sous la dir.
\RequirePackage[expert]{fourier}
\RequirePackage[scaled=0.875]{helvet}
\RequirePackage{courier}
\RequirePackage[utf8]{inputenc}

\RequirePackage[babel=true]{microtype}
\RequirePackage[a4paper,includemp=true]{geometry}
%\geometry{inner=2.625cm, outer=1.3125cm, marginparsep=0.65625cm, top=1.8561cm,height=24.13cm,textwidth=12.45cm,marginparwidth=3.9375cm}

\RequirePackage{xspace}% a mettre à la fin des macros
\RequirePackage{setspace}% pour changer les intelignes
    \widowpenalty=9999
    \clubpenalty=9999
\RequirePackage{rotating}
\RequirePackage{ragged2e}
\RequirePackage{marginnote}
\RequirePackage{xcolor}
\RequirePackage[modulo,running]{lineno}
\RequirePackage{csquotes} 
\RequirePackage{multicol}\setlength\columnsep{30pt}
\RequirePackage{floatpag}%Provides commands to apply different pagestyles to the full page floats.
\RequirePackage{graphicx}
\graphicspath{{\chemimage}}%chemin des images
\RequirePackage{float}
\RequirePackage{MnSymbol}%caracteres speciaux
\RequirePackage{textcomp}%caracteres speciaux
\RequirePackage{petitesmacros}
\RequirePackage{tikz}

\RequirePackage{ifthen}
\RequirePackage{ifoddpage}



\pagenumbering{gobble}

%%%%%%%%%%%%%%%%% CARACTERES SPECIAUX %%%%%%%%%%%%%%%%

\newcommand{\lienExterne}{%
    \tikz[x=1.2ex, y=1.2ex, baseline=-0.05ex]{% 
        \begin{scope}[x=1ex, y=1ex]
            \clip (-0.1,-0.1) 
                --++ (-0, 1.2) 
                --++ (0.6, 0) 
                --++ (0, -0.6) 
                --++ (0.6, 0) 
                --++ (0, -1);
            \path[draw, 
                line width = 0.6, 
                rounded corners=0.5] 
                (0,0) rectangle (1,1);
        \end{scope}
        \path[draw, line width = 0.6] (0.5, 0.5) 
            -- (1, 1);
        \path[draw, line width = 0.6] (0.6, 1) 
            -- (1, 1) -- (1, 0.6);
        }
    }


%%%%%%%%%%% TABLEAUX %%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{array}\renewcommand{\arraystretch}{1.5}
\RequirePackage{booktabs}% midrule toprule tableau sans lignes verticales
\newcommand\zpts{{\small0 -- 1 -- 2 -- 3 -- 4 -- 5}}

%%%%%%%%%%% Label des FIGURES %%%%%%%%%%%%%%%%%%%
\RequirePackage[labelfont=bf]{caption} %labels des images en gras
\addto\captionsfrench{% change le nom des légendes avec babel
    \renewcommand{\tablename}{Tableau}%
    \renewcommand{\figurename}{\small{Image}}%
}


%%%%%%%%%%%%%%%%%% BIBLIO %%%%%%%%%%%%%%%%%%%%%
\renewcommand{\newunitpunct}[0]{\addcomma\addspace}
\renewcommand{\subtitlepunct}[0]{\adddot\addspace}
%\DeclareFieldFormat[thesis]{title}{\textbf{#1}}
\DeclareFieldFormat[periodical]{date}{}%enlever parentheses vides

\newbibmacro{name:livrauteur}{% macro format de nom pour bookauthor
    \namepartprefix % la liste des choses à mettre 
    \textsc{\namepartfamily} 
    \namepartsuffix
}
\DeclareNameFormat{livrauteur}{%
    \nameparts{#1}%pas nécessaire dans de futures versions
    \usebibmacro{name:livrauteur}% on utilise la macro
    \usebibmacro{name:andothers}
}



\DefineBibliographyStrings{french}{andothers = {\em et\addabbrvspace al\adddot}}
\DefineBibliographyStrings{french}{pages = {pp\adddot}}
\DefineBibliographyStrings{french}{%
    byeditor = {\ifuseeditor{dir\adddotspace}{éd\adddot}},
}
\renewcommand*{\revsdnamepunct}{} %enlève la virgule entre NOM et Prénom
\renewcommand{\subtitlepunct}{\adddot\addspace}



\DeclareDataInheritance{book,article,periodical}{thesis}{
\inherit{author}{bookauthor}
\inherit{title}{booktitle}
\inherit{subtitle}{booksubtitle}
\inherit{titleaddon}{booktitleaddon}
\inherit{year}{userd}
\inherit{location}{lista}
\inherit{url}{url}
\noinherit{pubstate}
}

\DeclareBibliographyDriver{thesis}{%
    \usebibmacro{bibindex}%
    \usebibmacro{begentry}%
        %\renewcommand{\newunitpunct}[0]{\null}%citstyle verbose ajoute une virgule avan le titre
    \usebibmacro{title}%
    \newunit
    \printfield{titleaddon}%
    \newunit
    \printnames{author}%
    \newunit
    \usebibmacro{institution+location+date}%
    \newunit
    \printfield{pubstate}%
    \newunit
    \printfield{type}%
    \newunit
    \iffieldundef{booktitle}{}{%
        \printtext{in\addnbthinspace: }%
        \printnames[livrauteur][1-1]{bookauthor}%
        \newunit
        \usebibmacro{booktitle}% imprime automatiquement le booktitleaddon
        \newunit
        \iflistundef{location}{% si pas de lieu, on envoie le lieu d'édition
            \printlist{lista}\newunit}{}%
        \printfield{userd}%
        \newunit
        \printfield{pages}%
    }%
    \newunit
    \printfield{addendum}%
    \iffieldundef{url}{}%
    {\setunit{\hspace{1ex}}%
     \printtext{\href{\thefield{url}}{\lienExterne}}%
    }%
 }%


\DeclareBibliographyDriver{periodical}{% 
    \usebibmacro{bibindex}%
    \usebibmacro{begentry}%
    \usebibmacro{title}
    \newunit
    \usebibmacro{institution+location+date}
    \newunit
    \printfield{addendum}%
    \newunit
    \printfield{url} 
    \adddot
}

\renewbibmacro*{date}{%
    \printdate
    \iffieldundef{origyear}%
    {}%
    {%
    \setunit*{\addspace}%
    \printtext[parens]{\printorigdate}%
    }%
}

%%%%%%%%%%% IMAGES %%%%%%%%%%%%%%%%%

\newlength{\largim}%largeur des largimages
\setlength{\largim}{\textwidth}
\addtolength{\largim}{\marginparwidth}
\addtolength{\largim}{\marginparsep}

%normalimage
\newcounter{nim}
\newcommand\normalimage[2][]{%
    \begin{figure}[#1]%
        \includegraphics[width=\textwidth]{#2}%
        \caption{\small\cite{#2}}
    \end{figure}%
}
\newcommand\normalimagex[2][]{%
    \begin{figure}[#1]%
        \fbox{%
            \includegraphics[width=\textwidth]{#2}%
        }
        \caption{\small\cite{#2}}
    \end{figure}%
}

\newcommand\largimage[1]{%
    \begin{sidewaysfigure}
        \centering
        \includegraphics[width=\textwidth]{#1}
        \caption{\small\cite{#1}}
    \end{sidewaysfigure}
}

%margimage
\newcommand\margimage[2][0ex]{%
    \marginnote{%
        \includegraphics[width=\marginparwidth]{#2}
        \RaggedLeft\small\justifying\cite{#2}%
    }[#1]%vertical offset
}

\newcommand\margimagex[2][0ex]{% avec un cadre
    \marginnote{%
        \fbox{\includegraphics[width=\marginparwidth]{#2}%
        }%
        \\\raggedright\footnotesize\cite{#2}[]%vertical offset
    }%
}

%deuximages
\newcommand{\deuximages}[3][9]{%
    \begin{figure}[t]%
        \begin{minipage}{0.48\textwidth}%
            \marginnote{\small\justifying \textsb{A gauche\,:\\} \cite{#2}}%
            \includegraphics[width=\linewidth]{#2}%
        \end{minipage}\hfill%
        \begin{minipage}{0.48\textwidth}%
            \marginnote{\small\justifying \textsb{A droite\,:\\} \cite{#3}}[#1em]%
            \includegraphics[width=\linewidth]{#3}%
        \end{minipage}\hfill%
    \end{figure}%
}



\newcounter{lig}
\newcommand\largimagesvg[2][4ex]{%
    \stepcounter{lig}\label{lig-\thelig}%
    \ifthenelse{\isodd{\pageref{lil-\thelig}}}%
    {\begin{figure}[t]%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
    {\begin{figure}[t]%
        \hspace{-\marginparwidth}\hspace{-\marginparsep}%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}
        \marginnote{\RaggedRight\small\justifying\cite{#2}}[#1]%
    \end{figure}}%
}

\newcommand{\largimagesvgd}[2][4ex]{%
    {\begin{figure}[t]%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
}

%%%%%%%%%%% mise en page %%%%%%%%%%%%%%%%%

\newcommand{\italic}[1]{\vspace{-10pt}\subsection*{\rm\normalsize{\emph{#1}}}}
\newcommand{\ques}[1]{\noindent\textsb{#1}}

\newenvironment{texte}
{\begin{linenumbers*}}
{\end{linenumbers*}}

\newenvironment{textii}
{\begin{multicols}{2}\begin{linenumbers*}}
{\end{linenumbers*}\end{multicols}}

\newenvironment{refe}{%
\begin{spacing}{0.95}%
\medskip\small\noindent%
}{\end{spacing}}

%(marche pas)\newcommand{\refr}[2][]{\end{linenumbers*}\medskip\par\noindent\nopagebreak{\small\cite[#1]{#2}}\begin{linenumbers*}}

\renewenvironment{quotation} %mise en forme des citatitons longues
    {\list{}{\listparindent=15pt%whatever you need
        \itemindent    \listparindent
	%\small
        \leftmargin=12pt%  whatever you need
        \rightmargin=12pt%whatever you need
        %\topsep=0.3em%%%%%  whatever you need
        \parsep        \z@ \@plus\p@}%
        \item\relax}
    {\endlist}

%chronologie
\newenvironment{chrono}{%
    \setlength\columnsep{15pt}\footnotesize%
    \begin{multicols}{3}%
    }%
    {\end{multicols}}%

\RequirePackage{petitesmacros}

\RequirePackage[hidelinks]{hyperref}
\hypersetup{breaklinks=true,colorlinks=false,urlcolor=blue}

%exercices

%à faire un jour!

%%%%%%%%%%% TITRE %%%%%%%%%%%%%%%%%%%%%%%%%%

\date{} %pas de date ds le titre
\newcommand{\soustitre}{Le sous titre}
\newcommand{\titre}{Le titre}
\title{%     
    \begin{minipage}{1\linewidth}
        {\titre}%        
        \vskip3pt         
        \large \soustitre  
        \vskip3pt
        \small\RaggedLeft Signaler une erreur\,:\\ \href{mailto:pierre.pochon@vd.educanet2.ch?subject=ERREUR dans le fichier \jobname}{\texttt{pierre.pochon@vd.educanet2.ch}} 
    \end{minipage}%
} 
