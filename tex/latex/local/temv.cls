\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{te}[2018/05/10 v 0.3 travaux ecrits]% <--pas d'accents ici

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{exam}}
\ProcessOptions \relax

\LoadClass[a4paper,twoside,12pt,adpoints]{exam}
\input{../../chemins.tex}%adresse des références et images

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  \Titre pour un titre et                 %%
%%  \Date pour changer la date automatique  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[frenchb]{babel}
    \frenchbsetup{ThinColonSpace=true}
\RequirePackage[backend=biber,
    citestyle=verbose-trad2,
    bibstyle=verbose-trad3,
    url=false,
    ibidtracker=false]{biblatex}
    \addbibresource{../\bibliox}%chemin ref textes
    \addbibresource{../\imgbiblio}%chemin ref images
    \ExecuteBibliographyOptions[%
        reference]{useeditor=false}%dicos: sous la dir.
%MMV \RequirePackage[expert]{fourier}
%MMV \RequirePackage[scaled=0.875]{helvet}
%MMV \RequirePackage{courier}
%MMV \RequirePackage[utf8]{inputenc}

%%%%%%%% MODIFICATIONS MAL-VOYANTS (MMV) %%%%%%%%%%%%%%%%
\RequirePackage[17pt]{extsizes} %Pack Taille sépciale - 8pt, 9pt, 10pt, 11pt, 12pt, 14pt, 17pt et 20pt 
\usepackage{luatextra} % charge fontspec
\setromanfont{Verdana}
\setmonofont{DejaVu Sans Mono}
\newcommand{\textsb}[1]{% pas de Fourrier pas de textsb
     \textbf{#1}%
     }
\renewcommand{\textsc}[1]{% pas de Fourrier pas de smallcaps
     \MakeUppercase{#1}%
     }

\renewcommand{\baselinestretch}{1.2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\RequirePackage[babel=true]{microtype}
\RequirePackage{csquotes,xspace}
\RequirePackage{marginnote}
\RequirePackage{setspace}
\RequirePackage{graphicx}
    \graphicspath{{../\chemimage}}%chemin des images
\RequirePackage{ifthen}
\RequirePackage{xpatch}

\RequirePackage[a4paper]{geometry}
\geometry{lmargin={1in},
    tmargin={1in},
    rmargin={1.6in},
    bmargin={1in},
    marginparwidth={1in}, 
    marginparsep={0.2in}
}
\RequirePackage{tikz}
\usetikzlibrary{arrows,
    decorations.pathmorphing,
    decorations.markings,
    backgrounds,
    fit,
    positioning,
    shapes.symbols,
    chains,
    calc
}
\RequirePackage{multicol}
    \setlength\columnsep{30pt}
    %tweek https://tex.stackexchange.com/questions/404540/multicols-unwanted-vertical-space-in-the-right-column
        \patchcmd\multi@column@out
        {\process@cols}{%
           \typeout{Requested vsize = \the\dimen@ }%
           \advance\dimen@ -\topskip
           \divide\dimen@ \baselineskip
           \multiply\dimen@ \baselineskip
           \advance\dimen@ \topskip
           \typeout{Reducing vsize to integral number of lines = \the\dimen@ }%
           \process@cols}
        {\typeout{Success!}}{\ERROR}

\RequirePackage[modulo]{lineno}
    \renewcommand{\makeLineNumber}%
    {\llap{\linenumberfont\rlap{\LineNumber}\hspace{12pt}}}
\RequirePackage{enumitem} %listes
    \setdescription{leftmargin=0pt,labelindent=0pt}
    \setlist[enumerate,2]{label=\alph*)}
\RequirePackage{ragged2e} %coupures affinées
\RequirePackage{array}
    \renewcommand{\arraystretch}{1.5}% espaces dans les tableaux
    \addto\captionsfrench{% change le nom des légendes avec babel
    \renewcommand{\tablename}{Tableau}}
  
\widowpenalty=10000\clubpenalty=10000
\RequirePackage{amssymb}% pour les carrés dans les multiplechoice  
\checkboxchar{$\Box$} %voici les carrés

\RequirePackage{petitesmacros}
%%%%%%%%%%%%%%%%% CARACTERES SPECIAUX %%%%%%%%%%%%%%%%

\newcommand{\lienExterne}{%
    \tikz[x=1.2ex, y=1.2ex, baseline=-0.05ex]{% 
        \begin{scope}[x=1ex, y=1ex]
            \clip (-0.1,-0.1) 
                --++ (-0, 1.2) 
                --++ (0.6, 0) 
                --++ (0, -0.6) 
                --++ (0.6, 0) 
                --++ (0, -1);
            \path[draw, 
                line width = 0.6, 
                rounded corners=0.5] 
                (0,0) rectangle (1,1);
        \end{scope}
        \path[draw, line width = 0.6] (0.5, 0.5) 
            -- (1, 1);
        \path[draw, line width = 0.6] (0.6, 1) 
            -- (1, 1) -- (1, 0.6);
        }
    }


%%%%%%%%%%%%%%%%%%%%%%% HEAD et FOOT %%%%%%%%%%%%%%%%%%%%%
\pagestyle{headandfoot}\firstpageheadrule
\firstpageheader{\ifdef{\@Titre}%
    {\large\textsb{\@Titre}}{}}% gauche
    {}% centre
    {\@Date} %droite
\firstpagefooter%
    {}%g
    {\thepage/\numpages}%c
    {}%d
\runningfooter%
    {}%
    {\thepage/\numpages}%
    {\iflastpage{%
        \footnotesize\raggedleft \emph{total\,: \numpoints\ points}}
    }% droite

%%%%%%%%%%%%%%%%%%%%%%% TITRE et DATE %%%%%%%%%%%%%%%%%%%%
\newcommand{\Titre}[1]{\def\@Titre{#1}}
\newcommand{\@Date}{\printmonth\ \the\year}
\newcommand{\Date}[1]{\def\@Date{#1}}

\def\printmonth{\ifcase\month %pour avoir uniquement le mois avec \printmonth
    \or janvier\or f\'evrier\or mars\or avril\or mai\or juin\or
    juillet\or ao\^ut\or septembre\or octobre\or novembre\or
    d\'ecembre\fi \relax} 

%%%%%%%%%%%%%%%%%%%%%%%% BIBLIO  %%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{biblio}
%%%%%%%%%%%%%%%%%%%%%%% IMAGES %%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\floatpagefraction}{.8}%tolère de grandes images sans les isoler
%normalimage
\newcounter{nim}
\newcommand\normalimage[2][]{%
    \begin{figure}[#1]%
        \stepcounter{nim}\label{nim-\thenim}%
        \ifthenelse{\isodd{\pageref{nim-\thenim}}}%
        {\marginnote{\RaggedRight\small\cite{#2}}}%
        {\marginnote{\RaggedLeft\small\cite{#2}}}%
        \includegraphics[width=\textwidth]{../../IMG/#2}%
    \end{figure}%
}

\newcounter{nix}
\newcommand\normalimagex[2][]{%
    \begin{figure}[#1]%
        \stepcounter{nix}\label{nix-\thenix}%
        \ifthenelse{\isodd{\pageref{nix-\thenix}}}%
        {\marginnote{\RaggedRight\small\cite{#2}}}%
        {\marginnote{\RaggedLeft\small\cite{#2}}}%
        \fbox{\includegraphics[width=\textwidth]{../../IMG/#2}}%
    \end{figure}%
}

%deuximages
\newcommand{\deuximages}[2]{%
        \begin{minipage}{0.48\textwidth}%
            \includegraphics[width=\linewidth]{#1}%
            \smallskip
                \\\begin{minipage}{0.9\textwidth}
                    \small\cite{#1}
                \end{minipage}
        \end{minipage}\hfill%
        \begin{minipage}{0.48\textwidth}%
            \includegraphics[width=\linewidth]{#2}%
            \smallskip
                \\\begin{minipage}{0.9\textwidth}
                    {\small\cite{#2}}
                \end{minipage}
        \end{minipage}\hfill%
}

%%%%%%%%%%%%%%%%%%%%%%% COMMANDES  %%%%%%%%%%%%%%%%%%%%%5
\newcommand{\remarques}[1]{\null}
\newcommand{\Section}[1]{%pas d'indentation progressive
    \fullwidth{\section{#1}}
}
\newcommand{\italic}[1]{%
    \vspace{-10pt}\subsection*{\rm\normalsize{\emph{#1}}}
}
\newcommand{\lignes}[1]{% lignes sur toute la ligne
    \vspace{-10pt}\fullwidth{\fillwithlines{#1}}}

\newcommand{\lignex}{% /!\ NE FONCTIONNE PAS
    \begin{EnvFullwidth}
        \protect\fillwithlines{\stretch{1}}
    \end{EnvFullwidth}
    \newpage
}

\setlength\linefillheight{.40in} % espacement des lignes a écrire

\newenvironment{cit}{%
    \begin{linenumbers*}\begin{quotation}% début
}{\end{quotation}\end{linenumbers*}}% fin

\newenvironment{refe}{%
    \vspace{-5pt}%
    \begin{spacing}{0.95}%
    \medskip\small\noindent%
}{\end{spacing}}

\newcommand{\illus}[1]{%
    \fullwidth{%
        \fbox{\includegraphics[width=\linewidth]{#1}}
}{\vspace{-5pt}\fullwidth{\footnotesize\cite{#1}}\bigskip}}

\newcommand{\illu}[1]{%
    \fullwidth{\fbox{\includegraphics[width=\linewidth]{#1}}}\bigskip}

%un cadre pour dessiner (largeur en option? https://tex.stackexchange.com/questions/340878/empty-box-with-textwidth-and-specified-height 
\newcommand{\cadre}[2][\textwidth]{%
    \begingroup
    \setlength{\fboxsep}{-\fboxrule}%
    \fullwidth{\framebox[#1]{\rule{0pt}{#2}}}%
    \endgroup
}



\newenvironment{texte}{%
    \begin{linenumbers*}
}{\end{linenumbers*}}

%%%%%%%%%%%%%%%%%%%%%%% NUMEROTATION %%%%%%%%%%%%%%%%%%%
\renewcommand{\thesection}{\Alph{section}}%des capitales 

%%%%%%%%%%%%%%%%%%%%%%% HYPERREF %%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{hyperref}%marche pas si tout à la fin
    \hypersetup{breaklinks=true,
        colorlinks=false,
        urlcolor=blue}

%%%%%%%%%%%%%%%%%%%%%%% POINTS %%%%%%%%%%%%%%%%%%%%%%%%%
\AtBeginDocument{
    \remarques{}
    \begin{questions}
    \addpoints% dire de compter les points
}
\AtEndDocument{%
    \end{questions}
}

\bracketedpoints
%%%%%%%%%%%%%%%%%%%%%%% PACKAGE FINAUX %%%%%%%%%%%%%%%%%

