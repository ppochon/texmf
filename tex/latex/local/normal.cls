\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{corpus}[2017/06/02 v.03 Corpus de documents]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \documentclass{corpus}                   %%
%% \Titre{Mon titre}                        %%
%% \Soustitre{Mon soutitre}                 %%
%% \begin{document}                         %%
%% \Titrage                                 %% 
%% \end{document}                           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions \relax

\LoadClass[twoside,hidelinks,12pt]{article}

%\input{../chemins.tex} %adresse des images et bibfiles

\RequirePackage[french]{babel}%babel avant biblatex, sinon!
    \frenchbsetup{ThinColonSpace=true}%espaces fines après ":"
\RequirePackage[backend=biber,
    citestyle=verbose-trad2,
    bibstyle=verbose-trad2,
    url=true,ibidtracker=false, 
    idemtracker=false, 
    opcittracker=false,
    maxbibnames=2,
    dateuncertain=true,
    datecirca=true]{biblatex}
    %\addbibresource{\bibliox}%chemin ref textes
    %\addbibresource{\imgbiblio}%chemin ref images
\RequirePackage[expert]{fourier}
\RequirePackage[scaled=0.875]{helvet}
%\RequirePackage[scaled=0.975]{couriers}
\RequirePackage[scaled=0.975]{inconsolata}
\RequirePackage[utf8]{inputenc}
\RequirePackage[babel=true]{microtype}
\RequirePackage[a4paper,includemp=true]{geometry}
\geometry{inner=2.5cm,   %2.625cm, 
    outer=1.5cm, %1.375?
    marginparsep=0.7cm, 
    top=2.5cm,%1.8561cm,
    height=21.5cm, %24.13cm,
    textwidth=12cm,%12.45cm,
    marginparwidth=3.9375cm,
    %headheight=
    footskip=1.5cm}
\RequirePackage{xspace}% a mettre à la fin des macros
\RequirePackage{setspace}% pour changer les intelignes
%\RequirePackage{needspace}%éviter les ref sur l'autre page
\RequirePackage{fancyhdr}
\RequirePackage{ragged2e} %coupures affinées
\RequirePackage{marginnote} %notes marginales fixes
 \RequirePackage[modulo,running]{lineno}
     \renewcommand{\makeLineNumber}% espaceement hor. des numéros
     {\llap{\linenumberfont\rlap{\LineNumber}\hspace{15pt}}}
\RequirePackage{csquotes}%guimets français, \enquote 
\RequirePackage{multicol}
    \setlength\columnsep{30pt}
\RequirePackage{enumitem} %listes
    \setdescription{leftmargin=0pt,labelindent=0pt}
    \setlist[enumerate,2]{label=\alph*)}
\RequirePackage{float}
%\RequirePackage{floatpag}%pagestyles to the full page floats. ??
\RequirePackage{graphicx}
    %\graphicspath{{\chemimage}}%chemin des images
\RequirePackage{MnSymbol}%caracteres speciaux
\RequirePackage{textcomp}%caracteres speciaux
\RequirePackage{tikz}
\RequirePackage{xcolor} 

\RequirePackage{ifthen}
\RequirePackage{ifoddpage}
\RequirePackage{etoolbox}
\RequirePackage{xpatch}


\RequirePackage{petitesmacros}

\RequirePackage{url}
    %\urlstyle{same} %URLs en police normale 
    %%%%% césure des URLs (https://tex.stackexchange.com/questions/382888/bibliography-and-jabref)
    \setcounter{biburlnumpenalty}{8000}
    \setcounter{biburlucpenalty}{8000}  
    \setcounter{biburllcpenalty}{8000} 

\RequirePackage[hidelinks]{hyperref}
    \hypersetup{breaklinks=true,colorlinks=false,urlcolor=blue}

%%%%%%%%%%%%%%%%% HEADER FOOTER %%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\headrulewidth}{0pt}%supprimer la ligne en haut fancyhdr
\fancyhf{} %vide header et footer
%\fancyfootoffset{1cm} décale le footer
\fancyhead[LO,RE]{{\thepage}}%gauche impaire, droite paire
%\pagestyle{fancy}
%\g@addto@macro{\maketitle}{\thispagestyle{fancy}}% fancy aussi pour le titre

%%%%%%%%%%%%%%%%% CARACTERES SPECIAUX %%%%%%%%%%%%%%%%

\newcommand{\lienExterne}{%
    \tikz[x=1.2ex, y=1.2ex, baseline=-0.05ex]{% 
        \begin{scope}[x=1ex, y=1ex]
            \clip (-0.1,-0.1) 
                --++ (-0, 1.2) 
                --++ (0.6, 0) 
                --++ (0, -0.6) 
                --++ (0.6, 0) 
                --++ (0, -1);
            \path[draw, 
                line width = 0.6, 
                rounded corners=0.5] 
                (0,0) rectangle (1,1);
        \end{scope}
        \path[draw, line width = 0.6] (0.5, 0.5) 
            -- (1, 1);
        \path[draw, line width = 0.6] (0.6, 1) 
            -- (1, 1) -- (1, 0.6);
        }
    }

%%%%%%%%%%% TABLEAUX %%%%%%%%%%%%%%%

\RequirePackage{array}\renewcommand{\arraystretch}{1.5}
\addto\captionsfrench{% change le nom des légendes avec babel
    \renewcommand{\tablename}{Tableau}%
}

%%%%%%%%%%% BIBLIO %%%%%%%%%%%%%% biblatex >= 3.3
\RequirePackage{biblio} %normes biblographique générales pour les ouvrages et les références

%%%%%%%%%%% IMAGES %%%%%%%%%%%%%%%%%
\renewcommand{\floatpagefraction}{.8}%tolère de grandes images sans les isoler


%normalimage
\newcounter{nim}
\newcommand\normalimage[2][]{%
    \begin{figure}[#1]%
        \stepcounter{nim}\label{nim-\thenim}%
        \ifthenelse{\isodd{\pageref{nim-\thenim}}}%
        {\marginnote{\RaggedRight\small\cite{#2}}}%
        {\marginnote{\RaggedLeft\small\cite{#2}}}%
        \includegraphics[width=\textwidth]{#2}%
    \end{figure}%
}

\newcounter{nix}
\newcommand\normalimagex[1]{%
    \begin{figure}%
        \stepcounter{nix}\label{nix-\thenix}%
        \ifthenelse{\isodd{\pageref{nix-\thenix}}}%
        {\marginnote{\RaggedRight\small\cite{#1}}}%
        {\marginnote{\RaggedLeft\small\cite{#1}}}%
        \fbox{\includegraphics[width=\textwidth]{#1}}%
    \end{figure}%
}

%margimage
\newcommand\margimage[2][0ex]{\sloppy%
    \marginnote{%
        \includegraphics[width=\marginparwidth]{#2}
        \RaggedLeft\small\justifying\cite{#2}%
    }[#1]%vertical offset
}

\newcommand\margimagex[2][0ex]{% avec un cadre
    \marginnote{%
        \fbox{\includegraphics[width=\marginparwidth]{#2}%
        }%
        \\\raggedright\footnotesize\cite{#2}[]%vertical offset
    }%
}

%deuximages
\newcommand{\deuximages}[3][9]{%
    \begin{figure}[t]%
        \begin{minipage}{0.48\textwidth}%
            \marginnote{\small\justifying \textsb{A gauche\,:\\} \cite{#2}}%
            \includegraphics[width=\linewidth]{#2}%
        \end{minipage}\hfill%
        \begin{minipage}{0.48\textwidth}%
            \marginnote{\small\justifying \textsb{A droite\,:\\} \cite{#3}}[#1em]%
            \includegraphics[width=\linewidth]{#3}%
        \end{minipage}\hfill%
    \end{figure}%
}

%largimage
\newlength{\largim}%largeur des largimages
\setlength{\largim}{\textwidth}
\addtolength{\largim}{\marginparwidth}
\addtolength{\largim}{\marginparsep}

\newcounter{lim}
\newcommand\largimage[2][4ex]{%
    \stepcounter{lim}\label{li-\thelim}%
    \ifthenelse{\isodd{\pageref{lim-\thelim}}}%
    {\begin{figure}[t]%
        \vspace{-1.5cm}%
        \includegraphics[width=\largim]{#2}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
    {\begin{figure}[t]%
        \vspace{-1.5cm}%
        \hspace{-\marginparwidth}\hspace{-\marginparsep}%
        \includegraphics[width=\largim]{#2}%
        \marginnote{\RaggedRight\small\justifying\cite{#2}}[#1]%
    \end{figure}}%
}
 

\newcommand\largimaged[2][4ex]{%
    {\begin{figure}[t]%
        \vspace{-1.5cm}%
        \includegraphics[width=\largim]{#2}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
}

\newcommand\grandimage{\largimage} % dépréciée


\newcounter{lig}
\newcommand\largimagesvg[2][4ex]{%
    \stepcounter{lig}\label{lig-\thelig}%
    \ifthenelse{\isodd{\pageref{lil-\thelig}}}%
    {\begin{figure}[t]%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
    {\begin{figure}[t]%
        \hspace{-\marginparwidth}\hspace{-\marginparsep}%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}
        \marginnote{\RaggedRight\small\justifying\cite{#2}}[#1]%
    \end{figure}}%
}

\newcommand{\largimagesvgd}[2][4ex]{%
    {\begin{figure}[t]%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
}

%%%%%%%%%%% mise en page %%%%%%%%%%%%%%%%%
\widowpenalty=10000
\clubpenalty=10000

\newcommand{\italic}[1]{\vspace{-10pt}\subsection*{\rm\normalsize{\emph{#1}}}}
\newcommand{\ques}[1]{\noindent\textsb{#1}}

\newenvironment{texte}
{\begin{linenumbers*}}
{\end{linenumbers*}\nopagebreak}

\newenvironment{textii}
{\begin{multicols}{2}\begin{linenumbers*}}
{\end{linenumbers*}\end{multicols}}

\newenvironment{refe}{%
    \@beginparpenalty=100000
    \begin{spacing}{0.95}%
    \medskip\small\noindent%
}{\end{spacing}}

%(marche pas)\newcommand{\refr}[2][]{\end{linenumbers*}\medskip\par\noindent\nopagebreak{\small\cite[#1]{#2}}\begin{linenumbers*}}

\renewenvironment{quotation} %mise en forme des citations longues
    {\list{}{\listparindent=15pt%whatever you need
        \itemindent    \listparindent
	%\small
        \leftmargin=12pt%  whatever you need
        \rightmargin=12pt%whatever you need
        %\topsep=0.3em%%%%%  whatever you need
        \parsep        \z@ \@plus\p@}%
        \item\relax}
    {\endlist}

%chronologie
\newenvironment{chrono}{%
    \setlength\columnsep{15pt}\footnotesize%
    \begin{multicols}{3}%
    }%
    {\end{multicols}}%



%%%%%%%%%%% TITRE %%%%%%%%%%%%%%%%%%%%%%%%%%

\date{} %pas de date ds le titre
\providecommand{\Titrage}{\maketitle} %cohérence avec proj.cls
\title{%     
    \begin{minipage}{1\linewidth}%
        \ifdef{\@Titre}{\@Titre}{}% etoolbox.sty       
        \vskip3pt         
        \ifdef{\@Soustitre}{\large\@Soustitre}{}%        
        \vskip3pt
        \small\RaggedLeft Signaler une erreur\,:\\ \href{mailto:pierre.pochon@vd.educanet2.ch?subject=ERREUR dans le fichier \jobname}{\texttt{pierre.pochon@vd.educanet2.ch}} 
    \end{minipage}%
}
\newcommand{\Soustitre}{\newcommand{\@Soustitre}}
\newcommand{\Titre}{\newcommand{\@Titre}}


%%%%%%%%%%%% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%%%
\defbibfilter{livres}{type=book 
    or type=bookinbook
    or type=inbook}

\newcommand*{\Reff}{%
\section*{Bibliographie}
\nocite{*}
\printbibliography[title={Sources},keyword=source,notkeyword=stop]
\printbibliography[title={Références},keyword=ref,nottype=online]
\printbibliography[title={Sites web},keyword=ref,type=online]
}
%bibliographie par types d'ouvrage
\AtBeginBibliography{\small}%taille de la biblio
\newcommand*{\Reft}{%
\nocite{*}
\printbibliography[keyword=ref, filter=livres, title={Livres}]
\printbibliography[keyword=ref, type=incollection, title={Contributions dans un recueil}]
\printbibliography[keyword=ref, type=article, title={Articles de revues}]
\printbibliography[keyword=ref, type=inreference, title={Entrée d'un dictionnaire}]
\printbibliography[keyword=ref, type=online, title={Pages web}]
}

