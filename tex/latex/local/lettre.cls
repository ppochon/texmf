\NeedsTeXFormat{LaTeX2e}%/!\ pas fini
\ProvidesClass{lettre}[2017-02-06 lettre]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{chletter}}
\ProcessOptions \relax

\LoadClass[12pt]{chletter}
\RequirePackage[french]{babel}
    \frenchbsetup{ThinColonSpace=true}
\RequirePackage[expert]{fourier}
\RequirePackage[scaled=0.875]{helvet}
\RequirePackage{courier}
\RequirePackage[utf8]{inputenc}

\RequirePackage{csquotes}
\RequirePackage{inconsolata}

\RequirePackage{etoolbox}

\RequirePackage{xspace}

%\RequirePackage[babel=true]{microtype}
	\widowpenalty=9999
	\clubpenalty=9999
\RequirePackage[autolanguage]{numprint}


\RequirePackage{petitesmacros}

%\date{Lausanne, le \today}


