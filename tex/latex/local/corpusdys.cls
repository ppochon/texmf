\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{corpusmv}[2019/09/01 v.01 Corpus de documents pour K]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \documentclass{corpusmv}                 %%
%% \Titre{Mon titre}                        %%
%% \Soustitre{Mon soutitre}                 %%
%% \begin{document}                         %%
%% \Titrage                                 %%
%% \inputa[3ex]{lit_sec}                    %%
%% \inputm[5ex]{doc_margimage}              %%    
%% ...                                      %%    
%% \Ref                                     %%    
%% \Refk{keyword}                           %% 
%% \Refdoc{keyword}                         %%
%% \Table                                   %%
%% \end{document}                           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions \relax

\LoadClass[twoside,hidelinks,12pt]{article}

\input{../chemins.tex} %adresse des images et bibfiles

\RequirePackage[french]{babel}%babel avant biblatex, sinon!
    \frenchbsetup{ThinColonSpace=true}%espaces fines après ":"
\RequirePackage[backend=biber,
    datamodel=hist,
    bibstyle=verbose-trad2,
    citestyle=verbose-trad2,
    url=true,
    isbn=false,
    doi=false,
    ibidtracker=false, 
    idemtracker=false, 
    opcittracker=false,
    maxbibnames=2,
    dateuncertain=true,
    datecirca=true]{biblatex}
    \addbibresource{\bibliox}%chemin ref textes
    \addbibresource{\imgbiblio}%chemin ref images
    \addbibresource{docus.bib}% ~/texmf/bibtex/bib/docus.bib
    \NewBibliographyString{fromhebrew} %ajout pour les trad. de l'
    \DefineBibliographyStrings{french}{fromhebrew={de l'h\'{e}breu}}

%%%%%%%% MODIFICATIONS MAL-VOYANTS (MMV) %%%%%%%%%%%%%%%%
%MMV\RequirePackage[expert]{fourier}
%\RequirePackage[17pt]{extsizes} %Pack Taille sépciale - 8pt, 9pt, 10pt, 11pt, 12pt, 14pt, 17pt et 20pt 
\usepackage{luatextra} % charge fontspec
\setromanfont{Verdana}
\setmonofont{DejaVu Sans Mono}
\newcommand{\textsb}[1]{% pas de Fourrier pas de textsb
     \textbf{#1}%
     }
\renewcommand{\textsc}[1]{% pas de Fourrier pas de smallcaps
     \MakeUppercase{#1}%
     }

\renewcommand{\baselinestretch}{1.2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%MMV \RequirePackage[scaled=0.875]{helvet}
%MMV \RequirePackage[scaled=0.975]{couriers}
%MMV \RequirePackage[scaled=0.975]{inconsolata}
%MMV \RequirePackage[utf8]{inputenc}
\RequirePackage[babel=true]{microtype}
\RequirePackage[a4paper,includemp=true]{geometry}
\geometry{inner=2.5cm,   %2.625cm, 
    outer=1.5cm, %1.375?
    marginparsep=0.7cm, 
    top=2.5cm,%1.8561cm,
    height=21.5cm, %24.13cm,
    textwidth=13cm,%12.45cm,
    marginparwidth=3.9375cm,
    % marginparwidth=1.5cm,
    %headheight=
    footskip=1.5cm}
\RequirePackage{xspace}% a mettre à la fin des macros
\RequirePackage{setspace}% pour changer les intelignes
%\RequirePackage{needspace}%éviter les ref sur l'autre page
\RequirePackage{fancyhdr} %entêtes et pieds de pages
%\RequirePackage{tocloft} %customization de la table des matières
\RequirePackage{ragged2e} %coupures affinées
\RequirePackage{marginnote} %notes marginales fixes
\RequirePackage[modulo,running]{lineno}
     \renewcommand{\makeLineNumber}{\llap{\linenumberfont\rlap{\LineNumber}\hspace{15pt}}}
\RequirePackage{csquotes}%guimets français, \enquote 
\RequirePackage{multicol}
    \setlength\columnsep{30pt}
    %tweek https://tex.stackexchange.com/questions/404540/multicols-unwanted-vertical-space-in-the-right-column
        \patchcmd\multi@column@out
        {\process@cols}{%
           \typeout{Requested vsize = \the\dimen@ }%
           \advance\dimen@ -\topskip
           \divide\dimen@ \baselineskip
           \multiply\dimen@ \baselineskip
           \advance\dimen@ \topskip
           \typeout{Reducing vsize to integral number of lines = \the\dimen@ }%
           \process@cols}
        {\typeout{Success!}}{\ERROR}

\RequirePackage{etoc}%customisation de la table
\RequirePackage{enumitem} %listes
    \setdescription{leftmargin=0pt,labelindent=0pt}
    \setlist[enumerate,2]{label=\alph*)}
\RequirePackage{float}
\RequirePackage{floatpag}%pour enlever des numéros de pages
\RequirePackage{graphicx}
    \graphicspath{{\chemimage}}%chemin des images
%\RequirePackage{MnSymbol}%caracteres speciaux
\RequirePackage{textcomp}%caracteres speciaux
\RequirePackage{tikz}
\RequirePackage{xcolor} 

\RequirePackage{ifthen}
\RequirePackage{ifoddpage}
\RequirePackage{etoolbox}
\RequirePackage{xpatch}

\RequirePackage{petitesmacros}

\RequirePackage{url}
    %\urlstyle{same} %URLs en police normale 
    %%%%% césure des URLs (https://tex.stackexchange.com/questions/382888/bibliography-and-jabref)
    \setcounter{biburlnumpenalty}{8000}
    \setcounter{biburlucpenalty}{8000}  
    \setcounter{biburllcpenalty}{8000} 

%\RequirePackage{titlesec}
%%%%%%%%%%%%%%%%% HEADER FOOTER %%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\headrulewidth}{0pt}%supprimer la ligne en haut fancyhdr
\fancyhf{} %vide header et footer
%\fancyfootoffset{1cm} décale le footer
\fancyhead[LO,RE]{{\thepage}}%gauche impaire, droite paire

%%%%%%%%%%%%%%%%%% TITRE %%%%%%%%%%%%%%%%%%%%%%%%%%

\date{} %pas de date ds le titre
\providecommand{\Titrage}{\maketitle} %cohérence avec proj.cls
\title{%     
    \begin{minipage}{1\linewidth}%
        \ifdef{\@Titre}{\@Titre}{}% etoolbox.sty       
        \vskip3pt         
        \ifdef{\@Soustitre}{\large\@Soustitre}{}%        
        \vskip3pt
        \signalerreur
    \end{minipage}%
}

\newcommand{\Soustitre}{\newcommand{\@Soustitre}}
\newcommand{\Titre}{\newcommand{\@Titre}}
\newcommand{\signalerreur}{%
    \small\RaggedLeft Signaler une erreur\,:\\ \href{mailto:pierre.pochon@eduvaud.ch?subject=ERREUR dans le fichier \jobname}{\texttt{pierre.pochon@eduvaud.ch}} 
    }

%%%%%%%%%%%%%%%%%%%% MISE EN PAGE %%%%%%%%%%%%%%%%%

\widowpenalty=9999
\clubpenalty=9999

\newcommand{\italic}[1]{\vspace{-10pt}\subsection*{\rm\normalsize{\emph{#1}}}}
\newcommand{\ques}[1]{\noindent\textsb{#1}}

\newenvironment{texte}
{\begin{linenumbers*}}
{\end{linenumbers*}}%\nopagebreak}

\newenvironment{textii} %\parskip pour corriger le défaut de multicols %\setlength{\parskip}{0pt} plus nécessaire avec le tweak
{\begin{multicols}{2}\begin{linenumbers*}}
{\end{linenumbers*}\end{multicols}}

\newenvironment{refe}{%
\@beginparpenalty=10000%
\begin{spacing}{0.95}%DYS
\medskip\small\noindent%
}{\end{spacing}} % TRES BIZARRE ICI, mais ça marche comme cela…

\renewenvironment{quotation} %mise en forme des citations longues
    {\list{}{\listparindent=15pt%whatever you need
        \itemindent    \listparindent
	\small
        \leftmargin=20pt%  whatever you need
        \rightmargin=0pt%whatever you need
        %\topsep=0.3em%%%%%  whatever you need
        \parsep        \z@ \@plus\p@}%
        \item\relax}
    {\endlist}

\renewenvironment{quote} %mise en forme des citations courte
    {\list{}{\listparindent=0pt%whatever you need
        \itemindent    \listparindent
	\small
        \leftmargin=20pt%  whatever you need
        \rightmargin=0pt%whatever you need
        %\topsep=0.3em%%%%%  whatever you need
        \parsep        \z@ \@plus\p@}%
        \item\relax}
    {\endlist}

%%%%%%%%%%%%%%%%% SPECIAL INPUTS %%%%%%%%%%%%%%%%%%%%%%%%

\newlength{\zeroex} % longueur pour décaler les margimages
\setlength{\zeroex}{0ex}

%remonter les margimages dans les input. 
\newcommand{\inputm}[2][0ex]{%puis input pres redef de Zeroex
    {%un scope pour setlength
        \setlength{\zeroex}{#1}
        \input{#2}}
    }%fin du scope

% etoc pour identer les sect sans numéro https://tex.stackexchange.com/questions/434166/how-to-add-indent-in-non-numbered-section-of-the-table-of-content
\etocsetstyle{section}
    {}
    {\etocifnumbered
      {\etocsavedsectiontocline{\numberline{\etocnumber}\etocname}{\etocpage}}
      {\etocsavedsectiontocline{\numberline{}\etocname}{\etocpage}}%
    }
    {}
    {}
\etocsetstyle{subsection}
    {}
    {}
    {\etocsavedsubsectiontocline{\numberline{\etocnumber}\etocname}{\etocpage}}
    {}
\etocsetstyle{subsubsection}
    {}
    {}
    {\etocsavedsubsubsectiontocline{\numberline{\etocnumber}\etocname}{\etocpage}}
    {}

\newcommand{\inputa}[2][0ex]{%
    {%scope pour setlenght
    \small
    \setlength{\zeroex}{#1}%décalage margimage
    \renewenvironment{texte}%petite redef pour avoir deux colomnes
        {\begin{multicols}{2}\begin{linenumbers*}}%
        {\end{linenumbers*}\end{multicols}}%
    \setcounter{secnumdepth}{0} 
    \input{#2}
    \setcounter{secnumdepth}{2}
    \renewenvironment{texte}% retour à la def originale
        {\begin{linenumbers*}}%
        {\end{linenumbers*}}%
    }%fin du scope
}

%%%%%%%%%%%%%%%%% TABLE DES MATIERES %%%%%%%%%%%%%%%%

\renewcommand*\l@section{\@dottedtocline{1}{0em}{2.3em}}%des points après les sections dans la TOC https://tex.stackexchange.com/questions/53898/how-to-get-lines-with-dots-in-the-table-of-contents-for-sections 

\newcommand\Table{%
    \small\tableofcontents
    }

% \renewcommand\tableofcontents{% supprimer le titre de la table des mat
%     \@starttoc{toc}%
%     }

%%%%%%%%%%%%%%%%% CARACTERES SPECIAUX %%%%%%%%%%%%%%%%

\newcommand{\lienExterne}{%
    \tikz[x=1.2ex, y=1.2ex, baseline=-0.05ex]{% 
        \begin{scope}[x=1ex, y=1ex]
            \clip (-0.1,-0.1) 
                --++ (-0, 1.2) 
                --++ (0.6, 0) 
                --++ (0, -0.6) 
                --++ (0.6, 0) 
                --++ (0, -1);
            \path[draw, 
                line width = 0.6,                 %MMV 0.6 -> 1.1
                rounded corners=0.5] 
                (0,0) rectangle (1,1);
        \end{scope}
        \path[draw, line width = 0.6] (0.5, 0.5)  %MMV 0.6 -> 1.1
            -- (1, 1);
        \path[draw, line width = 0.6] (0.6, 1)    %MMV 0.6 -> 1.1 
            -- (1, 1) -- (1, 0.6);
        }
    }

%%%%%%%%%%% TABLEAUX %%%%%%%%%%%%%%%

\RequirePackage{array}
\renewcommand{\arraystretch}{1.5}
\addto\captionsfrench{% change le nom des légendes avec babel
    \renewcommand{\tablename}{Tableau}%
    }
\RequirePackage{booktabs}%\midrule etc.

%%%%%%%%%%% IMAGES %%%%%%%%%%%%%%%%%

\renewcommand{\floatpagefraction}{.8}%tolère de grandes images sans les isoler

\newcommand\margimage[2][\the\zeroex]{\sloppy%
    \marginnote{%
        \includegraphics[width=\marginparwidth]{#2}
        \RaggedLeft\small\justifying\cite{#2}%
        }[#1]%vertical offset
    }

%normalimage
\newcounter{nim}
\newcommand\normalimage[2][]{%
    \begin{figure}[#1]%
        \stepcounter{nim}\label{nim-\thenim}%
        \ifthenelse{\isodd{\pageref{nim-\thenim}}}%
        {\marginnote{\RaggedRight\small\cite{#2}}}%
        {\marginnote{\RaggedLeft\small\cite{#2}}}%
        \includegraphics[width=\textwidth]{#2}%
    \end{figure}%
    }

\newcounter{nix}
\newcommand\normalimagex[2][]{%
    \begin{figure}[#1]%
        \stepcounter{nix}\label{nix-\thenix}%
        \ifthenelse{\isodd{\pageref{nix-\thenix}}}%
        {\marginnote{\RaggedRight\small\cite{#2}}}%
        {\marginnote{\RaggedLeft\small\cite{#2}}}%
        \fbox{\includegraphics[width=\textwidth]{#2}}%
    \end{figure}%
    }

\newcommand\margimagex[2][\Zeroex]{% avec un cadre
    \marginnote{%
        \fbox{\includegraphics[width=\marginparwidth]{#2}%
        }%
        \\\raggedright\footnotesize\cite{#2}[]%vertical offset
        }%
    }

%colonimage
\newcommand{\colonimage}[1]{%
    \medskip
    \noindent%
    \begin{minipage}{\columnwidth}
        \centering%
        \marginnote{\small\cite{#1}}
        \includegraphics[width=\linewidth]{#1}
    \end{minipage}
    }

%deuximages
\newcommand{\deuximages}[3][9]{%
    \begin{figure}[t]%
        \begin{minipage}{0.48\textwidth}%
            \marginnote{\small\justifying \textsb{A gauche\,:\\} \cite{#2}}%
            \includegraphics[width=\linewidth]{#2}%
        \end{minipage}\hfill%
        \begin{minipage}{0.48\textwidth}%
            \marginnote{\small\justifying \textsb{A droite\,:\\} \cite{#3}}[#1em]%
            \includegraphics[width=\linewidth]{#3}%
        \end{minipage}\hfill%
    \end{figure}%
    }

%largimage
\newlength{\largim}%largeur des largimages
\setlength{\largim}{\textwidth}
\addtolength{\largim}{\marginparwidth}
\addtolength{\largim}{\marginparsep}

\newcounter{lim}
\newcommand\largimage[2][4ex]{%
    \stepcounter{lim}\label{li-\thelim}%
    \ifthenelse{\isodd{\pageref{lim-\thelim}}}%
    {\begin{figure}[t]%
        \vspace{-1.5cm}%
        \includegraphics[width=\largim]{#2}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
    {\begin{figure}[t]%
        \vspace{-1.5cm}%
        \hspace{-\marginparwidth}\hspace{-\marginparsep}%
        \includegraphics[width=\largim]{#2}%
        \marginnote{\RaggedRight\small\justifying\cite{#2}}[#1]%
    \end{figure}}%
    }
 
\newcommand\largimaged[2][4ex]{%
    {\begin{figure}[t]%
        \vspace{-1.5cm}%
        \includegraphics[width=\largim]{#2}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
    }

\newcommand\grandimage{\largimage} % dépréciée

\newcounter{lig}
\newcommand\largimagesvg[2][4ex]{%
    \stepcounter{lig}\label{lig-\thelig}%
    \ifthenelse{\isodd{\pageref{lil-\thelig}}}%
    {\begin{figure}[t]%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
    {\begin{figure}[t]%
        \hspace{-\marginparwidth}\hspace{-\marginparsep}%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}
        \marginnote{\RaggedRight\small\justifying\cite{#2}}[#1]%
    \end{figure}}%
    }

\newcommand{\largimagesvgd}[2][4ex]{%
    {\begin{figure}[t]%
        \def\svgwidth{\largim}%
        \input{../IMG/#2.pdf_tex}%
        \marginnote{\RaggedLeft\small\justifying\cite{#2}}[#1]
    \end{figure}}
    }

%%%%%%%%%% CHRONOLOGIE %%%%%%%%%%%%%%%%%%%

\newenvironment{chrono}
    {\setlength\columnsep{15pt}\small
    \begin{multicols}{3}}
    {\end{multicols}}%

%%%%%%%%%%%% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%%% biblatex > 3.3

\RequirePackage{biblio} %normes biblographique générales pour les ouvrages et les références

\defbibfilter{livres}{type=book 
    or type=bookinbook
    or type=inbook}

\AtBeginBibliography{\small}%taille de la biblio

\newcommand*{\Reff}{%
    %\section*{Bibliographie}
    \nocite{*}
    \printbibliography[title={Sources},keyword=source,notkeyword=stop]
    \printbibliography[title={Références},keyword=ref,nottype=online]
    \printbibliography[title={Sites web},keyword=ref,type=online]
    }

%Reference films documentaires
\newcommand*\Refdoc[1]{%
    \nocite{*}
    \printbibliography[title={Documentaires},keyword=#1,type=movie]
    }
    
%bibliographie par types d'ouvrage
\newcommand*{\Reft}{%
\nocite{*}
\printbibliography[keyword=ref, filter=livres, title={Livres}]
\printbibliography[keyword=ref, type=incollection, title={Contributions dans un recueil}]
\printbibliography[keyword=ref, type=article, title={Articles de revues}]
\printbibliography[keyword=ref, type=inreference, title={Entrée d'un dictionnaire}]
\printbibliography[keyword=ref, type=online, title={Pages web}]
}

%bibliographie par mots clés
\newcommand*{\Refk}[1]{%
\nocite{*}
\printbibliography[keyword=#1, nottype=online, title={Références}]
\printbibliography[keyword=#1, type=online, title={Sites web}]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[hidelinks]{hyperref}
    \hypersetup{breaklinks=true,
    colorlinks=false,
    urlcolor=blue,
    pdfencoding=unicode}
%This is the end, where hyperref belongs!



